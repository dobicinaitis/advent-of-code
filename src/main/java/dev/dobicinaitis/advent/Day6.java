package dev.dobicinaitis.advent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static dev.dobicinaitis.advent.Utils.getFileContentAsCharArray;

@Slf4j
@AllArgsConstructor
public class Day6 extends Puzzle {

    private static char GUARD = '^';
    private static char OBSTRUCTIONS = '#';
    private static char EMPTY_SPACE = '.';
    private static char VISITED = 'X';

    public Integer getFirstPuzzleSolution(String filename) {
        char[][] map = getFileContentAsCharArray(filename);
        Direction direction = Direction.UP;
        int[] guardPosition = findGuardPosition(map);
        log.debug("Guard position: ({}, {})", guardPosition[0], guardPosition[1]);
        Set<VisitedPosition> visitedPositions = new HashSet<>();
        visitedPositions.add(new VisitedPosition(guardPosition[0], guardPosition[1], direction));

        while (true) {
            if (isNextStepOutOfBounds(map, guardPosition, direction)) {
                log.debug("Leaving the area");
                break;
            }
            if (isNextStepObstructed(map, guardPosition, direction)) {
                log.debug("Turning right");
                direction = direction.turnRight();
                continue;
            }
            final UpdatedMap updatedMap = moveGuard(map, guardPosition, direction);
            map = updatedMap.map;
            guardPosition = updatedMap.guardPosition;
            visitedPositions.add(new VisitedPosition(guardPosition[0], guardPosition[1], direction));
            // showMap(map, visitedPositions);
        }

        log.debug("Visited positions: {}", Arrays.deepToString(visitedPositions.toArray()));

        return visitedPositions.size();
    }



    public Integer getSecondPuzzleSolution(String filename) {
//        char[][] map = getFileContentAsCharArray(filename);
//        Direction direction = Direction.UP;
//        int[] startingGuardPosition = findGuardPosition(map);
//        int[] guardPosition = startingGuardPosition;
//        Set<VisitedPosition> visitedPositions = new HashSet<>();
//        visitedPositions.add(new VisitedPosition(guardPosition[0], guardPosition[1], direction));
//        Set<List<Integer>> positionsToPlaceObstacles = new HashSet<>(); // positions where an obstacle could be placed to make the guard move in a loop
//
//        boolean isInStartingPosition = true;
//
//        while (true) {
//            if (isNextStepOutOfBounds(map, guardPosition, direction)) {
//                log.debug("Leaving the area");
//                break;
//            }
//            if (isNextStepObstructed(map, guardPosition, direction)) {
//                log.debug("Turning right");
//                direction = direction.turnRight();
//                continue;
//            }
//
//            if (!isInStartingPosition && !isNextStepObstructed(map, guardPosition, direction)) {
//                final Direction directionIfObstructed = direction.turnRight();
//                final boolean wouldEndUpInALoop = wouldTurningRightCauseALoop(map, guardPosition, directionIfObstructed, visitedPositions);
//                if (wouldEndUpInALoop) {
//                    log.debug("Guard would end up in a loop if turned right");
//                    final int[] positionToPlaceObstacle = calculateNextGuardPosition(guardPosition, direction);
//                    log.debug("Placing obstacle at ({}, {})", positionToPlaceObstacle[0], positionToPlaceObstacle[1]);
//                    positionsToPlaceObstacles.add(List.of(positionToPlaceObstacle[0], positionToPlaceObstacle[1]));
////                    showMap(map, visitedPositions, Set.of(List.of(positionToPlaceObstacle[0], positionToPlaceObstacle[1])));
////                    break;
//                }
//            }
//
//            isInStartingPosition = false;
//
//            final UpdatedMap updatedMap = moveGuard(map, guardPosition, direction);
//            map = updatedMap.map;
//            guardPosition = updatedMap.guardPosition;
//            visitedPositions.add(new VisitedPosition(guardPosition[0], guardPosition[1], direction));
//            //showMap(map, visitedPositions);
//
//
//        }
//
//        log.debug("Obstacle locations: {}", positionsToPlaceObstacles);
//
//        return positionsToPlaceObstacles.size();
        return 6;
    }

    private boolean wouldTurningRightCauseALoop(char[][] map, int[] guardPosition, Direction direction, Set<VisitedPosition> visitedPositions) {
        // check if by following this route the guard would end up in a loop
        Set<VisitedPosition> simulatedVisitedPositions = new HashSet<>(visitedPositions);
        int[] simulatedGuardPosition = Arrays.copyOf(guardPosition, guardPosition.length);
        Direction simulatedDirection = direction;

        while (true) {
            if (isNextStepOutOfBounds(map, simulatedGuardPosition, simulatedDirection)) {
                return false;
            }
            if (isNextStepObstructed(map, simulatedGuardPosition, simulatedDirection)) {
                simulatedDirection = simulatedDirection.turnRight();
                continue;
                //return false;
            }
            simulatedGuardPosition = calculateNextGuardPosition(simulatedGuardPosition, simulatedDirection);
            VisitedPosition newPosition = new VisitedPosition(simulatedGuardPosition[0], simulatedGuardPosition[1], simulatedDirection);

//            final UpdatedMap updatedMap = moveGuard(map, simulatedGuardPosition, simulatedDirection);
//            map = updatedMap.map;
//            simulatedGuardPosition = updatedMap.guardPosition;
//            VisitedPosition newPosition = new VisitedPosition(simulatedGuardPosition[0], simulatedGuardPosition[1], simulatedDirection);

            if (simulatedVisitedPositions.contains(newPosition)) {
//                log.debug("Simulation:");
//                showMap(map, simulatedVisitedPositions);
                return true;
            }
            simulatedVisitedPositions.add(newPosition);
        }
    }

    private boolean isNextStepOutOfBounds(char[][] map, int[] location, Direction direction) {
        int x = location[0] + direction.x;
        int y = location[1] + direction.y;
        return x < 0 || x >= map[1].length || y < 0 || y >= map.length;
    }

    private boolean isNextStepObstructed(char[][] map, int[] location, Direction direction) {
        int x = location[0] + direction.x;
        int y = location[1] + direction.y;
        return map[x][y] == OBSTRUCTIONS;
    }

    private int[] findGuardPosition(char[][] map) {
        for (int x = 0; x < map[0].length; x++) {
            for (int y = 0; y < map.length; y++) {
                if (map[x][y] == GUARD) {
                    return new int[]{x, y};
                }
            }
        }
        throw new RuntimeException("Guard not found");
    }

    private int[] calculateNextGuardPosition(int[] guardPosition, Direction direction) {
        return new int[]{guardPosition[0] + direction.x, guardPosition[1] + direction.y};
    }

    private UpdatedMap moveGuard(char[][] map, int[] guardPosition, Direction direction) {
        int[] newGuardLocation = new int[]{guardPosition[0] + direction.x, guardPosition[1] + direction.y};
        log.debug("Moved guard {} from {} to {}", direction, guardPosition, newGuardLocation);
        map[guardPosition[0]][guardPosition[1]] = VISITED;
        map[newGuardLocation[0]][newGuardLocation[1]] = GUARD;
        return new UpdatedMap(map, newGuardLocation);
    }

    private void showMap(char[][] map, Set<VisitedPosition> visitedPositions) {
        final int[][] visitedPositionsArray = visitedPositions.stream()
                .map(visitedPosition -> new int[]{visitedPosition.x, visitedPosition.y})
                .toArray(int[][]::new);
        log.debug("Map:\n{}", Utils.getColoredGrid(map, visitedPositionsArray));
    }

    private void showMap(char[][] map, Set<VisitedPosition> visitedPositions, Set<List<Integer>> positionsToPlaceObstacles) {
        for (List<Integer> position : positionsToPlaceObstacles) {
            map[position.get(0)][position.get(1)] = 'O';
        }

        final int[][] positionArray = Stream.concat(
                positionsToPlaceObstacles.stream()
                        .map(position -> new int[]{position.get(0), position.get(1)}),
                visitedPositions.stream()
                        .map(visitedPosition -> new int[]{visitedPosition.x, visitedPosition.y})
        ).toArray(int[][]::new);

        log.debug("Map:\n{}", Utils.getColoredGrid(map, positionArray));
    }


    record UpdatedMap(char[][] map, int[] guardPosition) {
    }

    record VisitedPosition(int x, int y, Direction direction) {
    }

    @AllArgsConstructor
    private enum Direction {
        UP(0, -1),
        RIGHT(1, 0),
        DOWN(0, 1),
        LEFT(-1, 0);

        private final int x;
        private final int y;

        public Direction turnRight() {
            return switch (this) {
                case UP -> RIGHT;
                case RIGHT -> DOWN;
                case DOWN -> LEFT;
                case LEFT -> UP;
            };
        }
    }
}
