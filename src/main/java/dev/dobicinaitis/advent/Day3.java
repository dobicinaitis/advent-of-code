package dev.dobicinaitis.advent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@AllArgsConstructor
public class Day3 extends Puzzle {

    public Integer getFirstPuzzleSolution(String filename) {
        final String input = Utils.getFileContentAsString(filename);

        return parseMultiplicationInstructions(input, false).stream()
                .mapToInt(instruction -> instruction.num1 * instruction.num2)
                .sum();
    }

    public Integer getSecondPuzzleSolution(String filename) {
        final String input = Utils.getFileContentAsString(filename);

        return parseMultiplicationInstructions(input, true).stream()
                .filter(MultiplicationInstruction::isEnabled)
                .mapToInt(instruction -> instruction.num1 * instruction.num2)
                .sum();
    }

    private List<MultiplicationInstruction> parseMultiplicationInstructions(String input, boolean checkEnabled) {
        final List<MultiplicationInstruction> occurrences = new ArrayList<>();
        final String regex = "mul\\((\\d){1,3},(\\d){1,3}\\)";
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            boolean isEnabled = !checkEnabled || isInstructionEnabled(input, matcher.start());
            occurrences.add(new MultiplicationInstruction(matcher.group(), isEnabled));
        }
        return occurrences;
    }

    private boolean isInstructionEnabled(String input, int startingPosition) {
        final String inputPortionToCheck = input.substring(0, startingPosition);
        final String enabledCondition = "do()";
        final String disabledCondition = "don't()";

        int lastEnabledIndex = inputPortionToCheck.lastIndexOf(enabledCondition);
        int lastDisabledIndex = inputPortionToCheck.lastIndexOf(disabledCondition);

        // enabled/true by default
        if (lastEnabledIndex == -1 && lastDisabledIndex == -1) {
            return true;
        }

        return lastEnabledIndex > lastDisabledIndex;
    }

    record MultiplicationInstruction(int num1, int num2, boolean isEnabled) {
        public MultiplicationInstruction(String instruction, boolean isEnabled) {
            this(
                    Utils.extractFirstNumberFromString(instruction.split(",")[0]),
                    Utils.extractFirstNumberFromString(instruction.split(",")[1]),
                    isEnabled
            );
        }
    }
}
