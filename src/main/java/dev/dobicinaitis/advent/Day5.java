package dev.dobicinaitis.advent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Slf4j
@AllArgsConstructor
public class Day5 extends Puzzle {

    public Integer getFirstPuzzleSolution(String filename) {
        final List<String> input = Utils.getFileContent(filename);
        final List<OrderRule> orderRules = parseOrderRules(input);
        final List<Updates> updates = parseUpdates(input);

        return updates.stream().parallel()
                .filter(update -> isUpdateValid(update.pages, orderRules))
                .mapToInt(update -> update.pages.get(update.pages.size() / 2)) // sum middle pages
                .sum();
    }

    public Integer getSecondPuzzleSolution(String filename) {
        final List<String> input = Utils.getFileContent(filename);
        final List<OrderRule> orderRules = parseOrderRules(input);
        final List<Updates> updates = parseUpdates(input);

        return updates.stream().parallel()
                .filter(update -> !isUpdateValid(update.pages, orderRules))
                .mapToInt(update -> findMiddlePageNumber(update.pages, orderRules))
                .sum();
    }

    private int findMiddlePageNumber(List<Integer> pages, List<OrderRule> orderRules) {
        final int pageCountTillMiddle = pages.size() / 2;
        for (int pageNumber : pages) {
            int numberOfPagesBefore = 0;
            for (OrderRule rule : getAppliedRules(pageNumber, pages, orderRules)) {
                if (rule.after == pageNumber) {
                    numberOfPagesBefore++;
                }
            }
            if (numberOfPagesBefore == pageCountTillMiddle) {
                return pageNumber;
            }
        }
        throw new RuntimeException("Could not compute middle page number");
    }

    private boolean isUpdateValid(List<Integer> pages, List<OrderRule> orderRules) {
        for (int pageIndex = 0; pageIndex < pages.size() - 1; pageIndex++) {
            final int pageNumber = pages.get(pageIndex);
            for (OrderRule rule : getAppliedRules(pageNumber, pages, orderRules)) {
                if (rule.before == pageNumber) {
                    final int afterIndex = pages.indexOf(rule.after);
                    if (afterIndex < pageIndex) {
                        return false;
                    }
                } else {
                    final int beforeIndex = pages.indexOf(rule.before);
                    if (beforeIndex > pageIndex) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private List<OrderRule> getAppliedRules(int currentPage, List<Integer> allPages, List<OrderRule> orderRules) {
        Set<Integer> allPagesSet = new HashSet<>(allPages);
        return orderRules.stream()
                .filter(rule -> (rule.before == currentPage && allPagesSet.contains(rule.after)) ||
                        (rule.after == currentPage && allPagesSet.contains(rule.before)))
                .toList();
    }

    private List<OrderRule> parseOrderRules(List<String> input) {
        return input.stream()
                .filter(line -> line.contains("|"))
                .map(OrderRule::new)
                .toList();
    }

    private List<Updates> parseUpdates(List<String> input) {
        return input.stream()
                .filter(line -> line.contains(","))
                .map(Updates::new)
                .toList();
    }

    record OrderRule(int before, int after) {
        public OrderRule(final String ruleString) {
            final String[] pages = ruleString.split("\\|");
            int before = Integer.parseInt(pages[0]);
            int after = Integer.parseInt(pages[1]);
            this(before, after);
        }
    }

    record Updates(List<Integer> pages) {
        public Updates(final String updateString) {
            List<Integer> pages = Arrays.stream(updateString.split(","))
                    .map(Integer::parseInt)
                    .toList();
            this(pages);
        }
    }
}
