package dev.dobicinaitis.advent;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
public class Day4 extends Puzzle {

    // Directions: [row_offset, col_offset]
    private static final int[][] DIRECTIONS = {
            {0, 1}, {0, -1},  // Horizontal (right, left)
            {1, 0}, {-1, 0},  // Vertical (down, up)
            {1, 1}, {-1, -1}, // Diagonal (primary)
            {1, -1}, {-1, 1}  // Diagonal (secondary)
    };

    public Integer getFirstPuzzleSolution(String filename) {
        final char[][] letterGrid = Utils.getFileContentAsCharArray(filename);
        final String wordToFind = "XMAS";
        return countWordOccurrences(letterGrid, wordToFind);
    }

    public Integer getSecondPuzzleSolution(String filename) {
        final char[][] letterGrid = Utils.getFileContentAsCharArray(filename);
        /*
          M.M      M.S      S.M      S.S
          .A.  or  .A.  or  .A.  or  .A.
          S.S      M.S      S.M      M.M
         */
        return countXmasOccurrences(letterGrid);
    }

    private int countXmasOccurrences(char[][] letterGrid) {
        final char crossPointLetter = 'A';
        final List<Character> edgeLetters = List.of('M', 'S');
        int count = 0;

        for (int row = 0; row < letterGrid[0].length - 2; row++) {
            for (int column = 0; column < letterGrid.length - 2; column++) {
                if (isXmasPattern(letterGrid, row, column, crossPointLetter, edgeLetters)) {
                    count++;
                }
            }
        }
        return count;
    }

    private boolean isXmasPattern(char[][] letterGrid, int row, int column, char crossPointLetter, List<Character> edgeLetters) {
        List<Character> availableLeftDiagonalLetters = new ArrayList<>(edgeLetters);  // ↘️
        List<Character> availableRightDiagonalLetters = new ArrayList<>(edgeLetters); // ↙️

        char upperLeftLetter = letterGrid[row][column];
        char upperRightLetter = letterGrid[row][column + 2];

        if (letterGrid[row + 1][column + 1] == crossPointLetter &&
                availableLeftDiagonalLetters.contains(upperLeftLetter) &&
                availableRightDiagonalLetters.contains(upperRightLetter)) {

            availableLeftDiagonalLetters.remove(Character.valueOf(upperLeftLetter));
            availableRightDiagonalLetters.remove(Character.valueOf(upperRightLetter));

            char lowerLeftLetter = letterGrid[row + 2][column];
            char lowerRightLetter = letterGrid[row + 2][column + 2];

            return availableLeftDiagonalLetters.contains(lowerRightLetter) && availableRightDiagonalLetters.contains(lowerLeftLetter);
        }
        return false;
    }

    private int countWordOccurrences(char[][] letterGrid, String wordToFind) {
        int count = 0;
        for (int row = 0; row < letterGrid[0].length; row++) {
            for (int column = 0; column < letterGrid.length; column++) {
                for (int[] direction : DIRECTIONS) {
                    if (findWord(letterGrid, wordToFind, row, column, direction)) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private boolean findWord(char[][] letterGrid, String wordToFind, int row, int column, int[] direction) {
        for (int letterIndex = 0; letterIndex < wordToFind.length(); letterIndex++) {
            int newRow = row + letterIndex * direction[0];
            int newColumn = column + letterIndex * direction[1];
            if (newRow < 0 || newRow >= letterGrid.length || newColumn < 0 || newColumn >= letterGrid[newRow].length) {
                return false;
            }
            if (letterGrid[newRow][newColumn] != wordToFind.charAt(letterIndex)) {
                return false;
            }
        }
        return true;
    }
}
