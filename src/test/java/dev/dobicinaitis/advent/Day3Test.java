package dev.dobicinaitis.advent;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day3Test {

    private final Puzzle puzzle = new Day3();

    @Test
    void firstPuzzleExampleTest() {
        assertEquals(161, puzzle.getFirstPuzzleSolution(puzzle.exampleInputFilename));
    }

    @Test
    void firstPuzzleMyInputTest() {
        assertEquals(169021493, puzzle.getFirstPuzzleSolution(puzzle.myInputFilename));
    }

    @Test
    void secondPuzzleExampleTest() {
        assertEquals(48, puzzle.getSecondPuzzleSolution("day3_example_input_2.txt"));
    }

    @Test
    void secondPuzzleMyInputTest() {
        assertEquals(111762583, puzzle.getSecondPuzzleSolution(puzzle.myInputFilename));
    }
}