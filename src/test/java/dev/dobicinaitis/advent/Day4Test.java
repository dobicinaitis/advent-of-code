package dev.dobicinaitis.advent;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day4Test {

    private final Puzzle puzzle = new Day4();

    @Test
    void firstPuzzleExampleTest() {
        assertEquals(18, puzzle.getFirstPuzzleSolution(puzzle.exampleInputFilename));
    }

    @Test
    void firstPuzzleMyInputTest() {
        assertEquals(2462, puzzle.getFirstPuzzleSolution(puzzle.myInputFilename));
    }

    @Test
    void secondPuzzleExampleTest() {
        assertEquals(9, puzzle.getSecondPuzzleSolution("day4_example_input_2.txt"));
    }

    @Test
    void secondPuzzleMyInputTest() {
        assertEquals(1877, puzzle.getSecondPuzzleSolution(puzzle.myInputFilename));
    }
}